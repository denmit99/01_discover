import java.net.*;
import java.io.*;
import java.util.*;

public class Main {
    static private final int TIMEOUT = 1000; //таймаут ожидания пакета
    static private final int RECVTIME = 5000; //время на получение пакетов
    static private final int TABLETIME = 5000;
    static private final int PORT = 4321;

    public static void main(String[] args) {

        try {
            if (args.length != 1) {
                System.out.println("Wrong number of arguments");
                return;
            }

            String multicastGroupAddress = args[0];
            InetAddress multicastGroup = InetAddress.getByName(multicastGroupAddress);
            MulticastSocket s = new MulticastSocket(PORT);
            s.setSoTimeout(TIMEOUT);
            s.joinGroup(multicastGroup);

            HashMap<String, Long> appInfo = new HashMap<>();
            while (true) {

                String msg = "hello";
                byte[] recvBytesBuf = new byte[msg.length()];
                DatagramPacket sendPacket = new DatagramPacket(msg.getBytes(), msg.length(), multicastGroup, PORT);
                DatagramPacket recvPacket = new DatagramPacket(recvBytesBuf, recvBytesBuf.length);
                s.send(sendPacket);
                long enterTime = System.currentTimeMillis();
                try {
                    do {
                        s.receive(recvPacket); //получить пакет
                        String senderIP = recvPacket.getAddress().toString();
                        if (appInfo.containsKey(senderIP)) { //действия с таблицей
                            appInfo.put(senderIP, System.currentTimeMillis());

                        } else {
                            appInfo.put(senderIP, System.currentTimeMillis());
                            printAppInfo(appInfo);
                        }
                    }
                    while (System.currentTimeMillis() - enterTime < RECVTIME);

                } catch (SocketTimeoutException e) {
                    //System.out.println("---");
                }
                if (deleteOld(appInfo))
                    printAppInfo(appInfo);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteOld(HashMap<String, Long> appInfo) {


        Iterator it = appInfo.entrySet().iterator();
        boolean isChanged = false;
        long currentTime = System.currentTimeMillis();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (currentTime - appInfo.get(pair.getKey()) > TABLETIME) {
                isChanged = true;
                it.remove();
            }
        }
        return isChanged;
    }

    public static void printAppInfo(HashMap<String, Long> appInfo) {

        Set<String> keys = appInfo.keySet();
        System.out.println("________TABLE_________");
        for (String c : keys) {
            System.out.println("[" + c + "]");
        }
        System.out.println("______________________\n");
    }
}
